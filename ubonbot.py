#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This code was written for Python 3.1.1
# version 0.101

# Changelog:
# version 0.100
# Basic framework
#
# version 0.101
# Fixed an error if an admin used a command with an argument, that wasn't an admin-only command

import socket, sys, threading, time
import random
import datetime
import pywapi
# Para el pastebin.ca
import urllib, urllib2, re

# Hardcoding the root admin - it seems the best way for now
root_admin = "yipman"

TYPES = [
"raw", "asterisk", "c", "cpp", "php", "perl", "java", "vb", "csharp",
"ruby", "python", "pascal", "mirc", "pli", "xml", "sql", "scheme",
"ascript", "ada", "apache", "nasm", "asp", "bash", "css", "delphi", "html", "js",
"lisp", "lua", "asm", "objc", "vbnet" ]

NAME_TYPE_DICT = {
"HTML"   : "html",   "CSS" : "css",    "JavaScript" : "js",    "sh"  : "bash", 
"Python" : "python", "PHP" : "php",    "Perl"       : "perl",  "SQL" : "sql",
"C"      : "c",      "Ada" : "ada",    "Lua"        : "lua",   "C++" : "cpp",
"Pascal" : "pascal", "C#"  : "csharp", "XML"        : "xml",
"VB.NET" : "vbnet",  "Java": "java",   "Ruby"       : "ruby" }

EXPIRATIONS = [
"", "5 minutes", "10 minutes", "15 minutes", "30 minutes", "45 minutes",
"1 hour", "2 hours", "4 hours", "8 hours", "12 hours", "1 day", "2 days",
"3 days", "1 week", "2 weeks", "3 weeks", "1 month", "2 months", "3 months",
"4 months", "5 months", "6 months", "1 year" ]

POSTDATA = "content=%(content)s&description=%(description)s&type=%(type)s&expiry=%(expiry)s&name=%(name)s&save=0&s=Submit+Post"
URL = "http://en.pastebin.ca/index.php"
URL_PATTERN = re.compile('http://en.pastebin.ca/\d+')

RAW_MODE = False

# Defining a class to run the server. One per connection. This class will do most of our work.
class IRC_Server:

    # The default constructor - declaring our global variables
    # channel should be rewritten to be a list, which then loops to connect, per channel.
    # This needs to support an alternate nick.
    def __init__(self, host, port, nick, channel , password =""):
	reload(sys)
	sys.setdefaultencoding('utf-8')
        self.irc_host = "irc.freenode.org"
        self.irc_port = 6665
        self.irc_nick = "ubonbot"
        self.irc_channel = "##archlinux-co"
        self.irc_sock = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )
        self.is_connected = False
        self.should_reconnect = False
        self.command = ""
	self.peleast = 0
	self.puntos1 = 50
	self.puntos2 = 50
	self.pr0n_ult = ""
	try:
	    # This tries to open an existing file but creates a new file if necessary.
	    self.logfile = open(str(datetime.date.today()) + ".txt", "a")
	except IOError:
	    pass
    
    # This is the bit that controls connection to a server & channel.
    # It should be rewritten to allow multiple channels in a single server.
    # This needs to have an "auto identify" as part of its script, or support a custom connect message.
    def connect(self):
        self.should_reconnect = True
        
        try:
            self.irc_sock.connect ((self.irc_host, self.irc_port))
        except:
            print ("Error: No se pudo conectar al IRC; Host: " + str(self.irc_host) + "Puerto: " + str(self.irc_port))
            exit(1) # We should make it recconect if it gets an error here
        print ("Conectado a: " + str(self.irc_host) + ":" + str(self.irc_port))
        
        str_buff = ("NICK %s \r\n") % (self.irc_nick)
        self.irc_sock.send (str_buff.encode())
        print ("Configurando el nick del bot a " + str(self.irc_nick) )
        
        str_buff = ("USER %s 8 * :X\r\n") % (self.irc_nick)
        self.irc_sock.send (str_buff.encode())
        print ("Configurando Usuario")
        # Insert Alternate nick code here.
        
        # Insert Auto-Identify code here.
        
        str_buff = ( "JOIN %s \r\n" ) % (self.irc_channel)
        self.irc_sock.send (str_buff.encode())
        print ("Uniendose al canal " + str(self.irc_channel) )
        self.is_connected = True
        self.listen()
    
    def listen(self):
        while self.is_connected:
            recv = self.irc_sock.recv( 4096)
            
            if str(recv).find ( "PING" ) != -1:
                self.irc_sock.send ( "PONG ".encode() + recv.split() [ 1 ] + "\r\n".encode() )
            if str(recv).find ( "PRIVMSG" ) != -1:
                irc_user_nick = str(recv).split ( '!' ) [ 0 ] . split ( ":")[1]
                irc_user_host = str(recv).split ( '@' ) [ 1 ] . split ( ' ' ) [ 0 ]
                self.irc_user_message = self.data_to_message(str(recv))
                print ( "[" + (str(recv)).split()[2] + "] " + irc_user_nick + ": " + self.irc_user_message)
		self.logfile.write(str(datetime.datetime.now()) + " [" +  (str(recv)).split()[2] + "] - " + irc_user_nick + ": " + self.irc_user_message + "\r\n")
                # "!" Indicated a command
                if ( str(self.irc_user_message[0]) == "!" ):
                    self.command = str(self.irc_user_message[1:])
                    # (str(recv)).split()[2] ) is simply the channel the command was heard on.
                    self.process_command(irc_user_nick, ( (str(recv)).split()[2] ) )
        if self.should_reconnect:
            self.connect()
    
    def data_to_message(self,data):
        data = data[data.find(':')+1:len(data)]
        data = data[data.find(':')+1:len(data)]
        data = str(data[0:len(data)-2])
        return data
        
    # This function sends a message to a channel, which must start with a #.
    def send_message_to_channel(self,data,channel):
        self.irc_sock.send( (("PRIVMSG %s :%s\r\n") % (channel, data)) )
    
    # This function takes a channel, which must start with a #.
    def join_channel(self,channel):
        if (channel[0] == "#"):
            str_buff = ( "JOIN %s \r\n" ) % (channel)
            self.irc_sock.send (str_buff.encode())
            # This needs to test if the channel is full
            # This needs to modify the list of active channels
            
    # This function takes a channel, which must start with a #.
    def quit_channel(self,channel):
        if (channel[0] == "#"):
            str_buff = ( "PART %s \r\n" ) % (channel)
            self.irc_sock.send (str_buff.encode())
            # This needs to modify the list of active channels
    
    def randomLine(self,filename):
        "Retrieve a  random line from a file, reading through the file once"
	fh = open(filename, "r")
	lineNum = 0
	it = ''

	while 1:
		aLine = fh.readline()
		lineNum = lineNum + 1
		if aLine != "":
			#
			# How likely is it that this is the last line of the file ? 
			if random.uniform(0,lineNum)<1:
				it = aLine
		else:
			break

	fh.close()

	return it

    def pastebin(self, content, description="", name="", expiry="", type="raw"):
	payload = POSTDATA % {
		"content" : urllib.quote_plus(content),
		"description" : urllib.quote_plus(description),
		"type" : TYPES.index(type)+1,
		"expiry" : urllib.quote_plus(expiry),
		"name" : urllib.quote_plus(name)
		}
	req = urllib2.Request(URL, payload, {
		"Referer": "http://en.pastebin.ca/",
		"Content-Type": "application/x-www-form-urlencoded",
		"User-Agent": "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050813 Epiphany/1.7.6",
		"Accept-Language": "en",
		"Accept-Charset": "utf-8",
		})
	return urllib2.urlopen(req).read()

    def got_page(self, res):
	matches = URL_PATTERN.findall(res)
	if len(matches) > 0:
		match = matches[0]
		if RAW_MODE:
			return match.replace("pastebin.ca/","pastebin.ca/raw/")
		else:
			return match
	else:
		sys.stderr.write("Could not retreive pastebin url\n")

    def userLine(self,filename, perfil):
        "Devuelve la linea con el perfil solicitado"
	fh = open(filename, "r")
	lineNum = 0
	it = ''

	while 1:
		aLine = fh.readline()
		lineNum = lineNum + 1
		if aLine != "":
			#
			# How likely is it that this is the last line of the file ? 
			index = aLine.find(perfil)
			if index > -1 and index < 5:
				it = aLine
		else:
			break

	fh.close()

	return it

    # This nice function here runs ALL the commands.
    # For now, we only have 2: root admin, and anyone.
    def process_command(self, user, channel):
        # This line makes sure an actual command was sent, not a plain "!"
        if ( len(self.command.split()) == 0):
            return
        # So the command isn't case sensitive
        command = (self.command).lower()
        # Break the command into pieces, so we can interpret it with arguments
        command = command.split()
        
        # All admin only commands go in here.
        if (user == root_admin):
            # The first set of commands are ones that don't take parameters
            if ( len(command) == 1):

                #This command shuts the bot down.    
                if (command[0] == "quit"):
		    self.logfile.close()
                    str_buff = ( "QUIT %s \r\n" ) % (channel)
                    self.irc_sock.send (str_buff.encode())
                    self.irc_sock.close()
                    self.is_connected = False
                    self.should_reconnect = False
                    
            # These commands take parameters
            else:
                
                # This command makes the bot join a channel
                # This needs to be rewritten in a better way, to catch multiple channels
                if (command[0] == "join"):
                    if ( (command[1])[0] == "#"):
                        irc_channel = command[1]
                    else:
                        irc_channel = "#" + command[1]
                    self.join_channel(irc_channel)
                    
                # This command makes the bot part a channel
                # This needs to be rewritten in a better way, to catch multiple channels
                if (command[0] == "part"):
                    if ( (command[1])[0] == "#"):
                        irc_channel = command[1]
                    else:
                        irc_channel = "#" + command[1]
                    self.quit_channel(irc_channel)

                
        # All public commands go here
        # The first set of commands are ones that don't take parameters
        if (command[0] == "temperatura"):
	    if len(command) == 3:
	  	ciudad = command[1]+' '+command[2]
	  	google_result = pywapi.get_weather_from_google(command[1]+'+'+command[2])
		  
	    if len(command) == 2:
		ciudad = command[1]
	  	google_result = pywapi.get_weather_from_google(ciudad)
		  
	    if len(command) == 1:
	  	self.send_message_to_channel( (user + ": tenes que escribir el nombre de la ciudad de la que queres saber la temperatura..."), channel )
	  	return
	  	
	    if len(command) > 3:
		self.send_message_to_channel( (user + ": no quiero decirte la temperatura de esa ciudad :D problema?"), channel )
		return
		
	    if (len(google_result['current_conditions']) > 0):
		  temp_c = google_result['current_conditions']['temp_c']
		  self.send_message_to_channel( (user + ": en este momento la temperatura en " + ciudad + " es de " + temp_c + " grados celcius."), channel )
	    else:
		  self.send_message_to_channel( (user + ": no pude recuperar la temperatura de " + ciudad + "."), channel )
		 
        
        if ( len(command) == 1):
        
            if (command[0] == "ast"):
		ast = str(self.randomLine("baseast.txt")) # random
		self.send_message_to_channel( (user + ": " + str(ast[0:len(ast)-1])), channel )

            if (command[0] == "ayuda"):
		self.send_message_to_channel( (user + ": no tengo ayuda pedazo de imberbe."), channel )
		
	    if (command[0] == "fefe"):
		self.send_message_to_channel( (user + ": para jugar a Donde está el fefe? entrá a http://imageshack.us/photo/my-images/402/dondefin.jpg/."), channel )

            if (command[0] == "comandos"):
		self.send_message_to_channel( (user + ": los comandos que podes usar son !ast !ayuda !comandos !helpo !agregar !cock !perfil !google !decir !sod."), channel )

            if (command[0] == "helpo"):
		self.send_message_to_channel( (user + ": a mi me venis a hablar en esperanto?"), channel )

            if (command[0] == "cock"):
		self.send_message_to_channel( (user + ": en #archlinux-co (aka jumanji) no hay reglas ni codigos de conducta, si no se adapta a la jungla mejor andate a otro canal vos!"), channel )

            if (command[0] == "radio"):
		self.send_message_to_channel( (user + ": sintoniza Radio Jumanji, la radio oficial de #archlinux-co! http://archlinux.co.cc/radio.pls , con el comando !jumanji te digo que estamos pasando."), channel )

	if ( len(command) == 2):
		    if (command[0] == "ast"):
			ast = str(self.randomLine("baseast.txt")) # random
		        self.send_message_to_channel( (command[1] + ": " + str(ast[0:len(ast)-1])), channel )

           	    if (command[0] == "cock"):
			self.send_message_to_channel( (command[1] + ": en #archlinux-co (aka jumanji) no hay reglas ni codigos de conducta, si no te adaptas a la jungla mejor andate a otro canal!"), channel )

           	    if (command[0] == "rlog"):
			content = ""
			paste = ""
			type = "raw"
			start = 1
			end = 0
			f = None
			try:
				f = file(command[1] + ".txt", 'r')

				lines = None
				if end == 0:
					lines = f.readlines()[start-1:]
				else:
					lines = f.readlines()[start-1:end]
			
				content = ''.join(lines)
				f.close()

				res = self.pastebin(content, "Log del " + command[1] + " en " + channel, command[1], "", type)
				paste = self.got_page(res)
				self.send_message_to_channel( (user + ": se ha subido a pastebin el log del " + command[1] + " a la url " + paste), channel )

			except Exception, msg:
				self.send_message_to_channel( (user + ": error intentando leer el log del " + command[1]), channel )
			

           	    if (command[0] == "pr0n"):
	           	    if (command[1].find("http://") != -1):
				self.send_message_to_channel( (channel + " vota por este link de pr0n con !pr0n ++"), channel )
				self.pr0n_ult = command[1]

	           	    if (command[1] == "++"):
	           	    	if self.pr0n_ult != "":
					self.send_message_to_channel( (user + " acaba de votar positivamente el link de pr0n " + self.pr0n_ult ), channel )
				else:
					self.send_message_to_channel( (user + ": que queres votar, si nadie roto pr0n?"), channel )

	           	    if (command[1] == "--"):
	           	    	if self.pr0n_ult != "":
					self.send_message_to_channel( (user + " acaba de votar negativamente el link de pr0n " + self.pr0n_ult ), channel )
	           	    	else:
					self.send_message_to_channel( (user + ": que queres votar, si nadie roto pr0n?"), channel )

           	    if (command[0] == "peleast"):
	           	    if (command[1] == "fin") and self.peleast == 1:
				self.send_message_to_channel( ("Pelea abortada por " + user), channel )
				self.peleast = 0

           	    if (command[0] == "perfil"):
			try:
				perfil = str(self.userLine("perfiles.txt", command[1])) # random
			        self.send_message_to_channel( (str(perfil[0:len(perfil)-1])), channel )
			except IOError:
			        pass
		
	            if (command[0] == "google"):
			self.send_message_to_channel( (user + ": me viste cara de mctpyt? entra a google y busca sobre " + command[1] + " por tu cuenta, inutil!"), channel )

		    if (command[0] == "radio"):
			self.send_message_to_channel( (command[1] + ": sintoniza Radio Jumanji, la radio oficial de #archlinux-co! http://archlinux.co.cc/radio.pls , con el comando !jumanji te digo que estamos pasando."), channel )
		
		    if (command[0] == "sod"):
			  if len(command) == 1:
			    self.send_message_to_channel( ("\x01ACTION sodomiza a " + user + "\x01"), channel )
			    
			  if len(command) > 1:
			    self.send_message_to_channel( ("\x01ACTION sodomiza a " + str(command[1]) + "\x01"), channel )

	if ( len(command) > 2):
	  
		if self.peleast == 1 and command[0] == "peleast":			
			if user == self.peleast1 and self.irc_user_message != self.peleast_ult1:
				i = 1
				puntos = 0
				self.peleast_ult1 = self.irc_user_message
				while i != len(command) and i != 11:
					nvospuntos = 0
					if command[i] == "bondiolita":
						nvospuntos = 5
					if command[i] == "carita":
						if command[i+1] == "de":
							if command[i+2] == "bondiola":
								nvospuntos = 7
					if command[i] == "tetas":
						if command[i+1] == "de":
							if command[i+2] == "katai":
								nvospuntos = 6
					if command[i] == "bolas":
						if command[i] == "de":
							if command[i] == "toro":
								nvospuntos = 4
					if command[i] == "culo":
						if command[i] == "de":
							if command[i] == "niwi":
								nvospuntos = 6
					if command[i] == "queso":
						if command[i] == "rayado":
								nvospuntos = 4
						if command[i] == "parmesano":
								nvospuntos = 6
						if command[i] == "de":
							if command[i] == "cabra":
								nvospuntos = 7
					if command[i] == "vos":
						nvospuntos = 1
					if command[i] == "bubuntu":
						nvospuntos = 3
					if command[i] == "tetasdeniwi":
						nvospuntos = 4
					if command[i] == "tenes":
						nvospuntos = 1

					puntos = puntos + nvospuntos
					i = i+1

				self.puntos2 = self.puntos2 - puntos
				if self.puntos2 <= 0:
					#gameover
					self.send_message_to_channel( ("Pelea terminada."), channel )
					self.send_message_to_channel( ("El ganador es ..."), channel )
					self.send_message_to_channel( (self.peleast1), channel )
					self.peleast = 0

				else:
					self.send_message_to_channel( ("Vitalidad: " + self.peleast1 + " " + str(self.puntos1) + "pts - " + self.peleast2 + " " + str(self.puntos2) + "pts"), channel )

			if user == self.peleast2 and self.irc_user_message != self.peleast_ult2:
				i = 1
				puntos = 0
				self.peleast_ult2 = self.irc_user_message
				while i != len(command) and i != 11:
					nvospuntos = 0
					if command[i] == "bondiolita":
						nvospuntos = 5
					if command[i] == "carita":
						if command[i+1] == "de":
							if command[i+2] == "bondiola":
								nvospuntos = 7
					if command[i] == "tetas":
						if command[i+1] == "de":
							if command[i+2] == "katai":
								nvospuntos = 6
					if command[i] == "bolas":
						if command[i] == "de":
							if command[i] == "toro":
								nvospuntos = 4
					if command[i] == "culo":
						if command[i] == "de":
							if command[i] == "niwi":
								nvospuntos = 6
					if command[i] == "queso":
						if command[i] == "rayado":
								nvospuntos = 4
						if command[i] == "parmesano":
								nvospuntos = 6
						if command[i] == "de":
							if command[i] == "cabra":
								nvospuntos = 7
					if command[i] == "vos":
						nvospuntos = 1
					if command[i] == "bubuntu":
						nvospuntos = 3
					if command[i] == "tetasdeniwi":
						nvospuntos = 4
					if command[i] == "tenes":
						nvospuntos = 1

					puntos = puntos + nvospuntos
					i = i+1

				self.puntos1 = self.puntos1 - puntos
				if self.puntos1 <= 0:
					#gameover
					self.send_message_to_channel( ("Pelea terminada."), channel )
					self.send_message_to_channel( ("El ganador es ..."), channel )
					self.send_message_to_channel( (self.peleast2), channel )
					self.peleast = 0
				else:
					self.send_message_to_channel( ("Vitalidad: " + self.peleast1 + " " + str(self.puntos1) + "pts - " + self.peleast2 + " " + str(self.puntos2) + "pts"), channel )

		else:
			if (command[0] == "peleast"):
				self.send_message_to_channel( (user + ": Comienza la batalla en baseast, participantes " + command[1] + " y " + command[2] + ". Ambos empiezan con 50 puntos de vitalidad. Frases en base ast con un minimo de 2 palabras y un maximo de 10 palabras."), channel )

				self.peleast1 = command[1]
				self.peleast2 = command[2]
				self.puntos1 =50 
				self.puntos2 =50
				self.peleast_ult1 = ""
				self.peleast_ult2 = ""
				self.peleast = 1

				self.send_message_to_channel( (user + ": Preparados"), channel )
				self.send_message_to_channel( (user + ": Listos"), channel )
				self.send_message_to_channel( (user + ": Ya!"), channel )

			    
			if (command[0] == "agregar"):
				if (command[1] == "ast"):
					i = 2
					texto = ""
					while i != len(command):
						texto = texto + " " + command[i]
						i = i+1

					try:
					    # This tries to open an existing file but creates a new file if necessary.
					    logfile = open("baseast.txt", "a")
					    try:
						logfile.write(texto + "\r\n")
					    finally:
						logfile.close()
					except IOError:
					    pass


			
				if (command[1] == "perfil"):
					try:
						perfil = str(self.userLine("perfiles.txt", command[2]))
						if perfil != "":
							self.send_message_to_channel( (user + ": ya existe un perfil para " + command[2]), channel )

						else:
							i = 3
							texto = command[2] + ": "
							while i != len(command):
								texto = texto + " " + command[i]
								i = i+1

							try:
							    # This tries to open an existing file but creates a new file if necessary.
							    logfile = open("perfiles.txt", "a")
							    try:
								logfile.write(texto + "\r\n")
							    finally:
								logfile.close()
							except IOError:
							    pass
						
					except IOError:
						pass



			if (command[0] == "decir"):
				i = 3
				texto = ""
				while i != len(command):
					texto = texto + " " + command[i]
					i = i+1
				self.send_message_to_channel( (command[2] + ":" + texto), command[1] )
            

# Here begins the main programs flow:

#test2 = IRC_Server("irc.irchighway.net", 6667, "masbot", "#test")
test = IRC_Server("irc.freenode.org", 6667, "ubon", "##archlinux-co")
run_test = threading.Thread(None, test.connect)
run_test.start()

while (test.should_reconnect):
    time.sleep(5)
